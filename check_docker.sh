 check_running_docker (){
              container_id=$(docker ps | grep gitlab-djpass:latest | awk '{print $1}')
              if [ -z "$container_id" ]; then
                echo "All Clean no such container"
              else
                
                echo "Running Docker $container_id, killing it"
                docker stop djpassv1
                docker rm -f djpassv1
                docker image rm -f  gitlab-djpass
              fi
        }
check_running_docker;
